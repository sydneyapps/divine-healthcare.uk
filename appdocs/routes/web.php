<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/about', function () {
    return view('about.about');
});
Route::get('/testimonials', function () {
    return view('testimonials.testimonials');
});
Route::get('/about/general-information', function () {
    return view('about.general-information');
});
Route::get('/downloads/{download}', 'downloadController@index');

Route::get('/download/{f}/{e}', 'downloadController@download');

Route::get('/healthcare/services', function () {
    return view('healthcare.services');
});

Route::get('/healthcare/recovery-convalescent', function () {
    return view('healthcare.recovery-convalesant');
});

Route::get('/healthcare/companionship-and-sitting-services', function () {
    return view('healthcare.companionship-and-sitting-services');
});

Route::get('/healthcare/personal-care', function () {
    return view('healthcare.personal-care');
});

Route::get('/healthcare/house-keeping', function () {
    return view('healthcare.house-keeping');
});

Route::get('/healthcare/care-packages', function () {
    return view('healthcare.care-packages');
});

Route::get('/healthcare/live-in-care', function () {
    return view('healthcare.live-in-care');
});

Route::get('/healthcare/round-the-clock-care', function () {
    return view('healthcare.round-the-clock-care');
});

Route::get('/healthcare/sleeping-night', function () {
    return view('healthcare.sleeping-night');
});

Route::get('/healthcare/respite-care', function () {
    return view('healthcare.respite-care');
});

Route::get('/healthcare/personalisation', function () {
    return view('healthcare.personalisation');
});

Route::get('/healthcare/care-funding', function () {
    return view('healthcare.care-funding');
});

Route::get('/healthcare/social-services-funding', function () {
    return view('healthcare.social-services-funding');
});

Route::get('/healthcare/direct-payments', function () {
    return view('healthcare.direct-payments');
});

Route::get('/healthcare/self-funding', function () {
    return view('healthcare.self-funding');
});

Route::get('/healthcare/nhs-continuing-care', function () {
    return view('healthcare.nhs-continuing-care');
});

Route::get('/healthcare/in-control', function () {
    return view('healthcare.in-control');
});

Route::get('/healthcare/personal-budget', function () {
    return view('healthcare.personal-budget');
});

Route::get('/supported-living/about', function () {
    return view('supported-living.about');
});

Route::get('/supported-living/transition-support', function () {
    return view('supported-living.transition-support');
});

Route::get('/supported-living/outreach-support', function () {
    return view('supported-living.outreach-support');
});
Route::get('/supported-living/recovery', function () {
    return view('supported-living.recovery');
});
Route::get('/supported-living/accommodation', function () {
    return view('supported-living.accommodation');
});
Route::get('/supported-living/learning-disabilities', function () {
    return view('supported-living.learning-disabilities');
});
Route::get('/education/qcf-training', function () {
    return view('education.qcf-training');
});
Route::get('/education/apprenticeship', function () {
    return view('education.apprenticeship');
});
Route::get('/education/loan-for-24-plus', function () {
    return view('education.loan-for-24-plus');
});

Route::get('/education/student-support', function () {
    return view('education.student-support');
});

Route::get('/contact-us', function () {
    return view('contact.us');
});

Route::get('/recruitment/careers', function () {
    return view('recruitment.careers');
});

Route::get('/recruitment/online-application', function () {
    return view('recruitement.online-application');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
