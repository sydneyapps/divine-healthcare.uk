@extends('layouts.page')
@section('page-title')
Divine Healthcare  : Support Support - Accommodation
@endsection
@section('article-title')
Support service : Learning disabilities
@endsection
@section('article-body')

<article>
<p>We specialise in providing services to people with learning disabilities. The service users range from low to complex dependent. The delivery of support services is to help the service users so as to improve their mental wellbeing. The delivery considers the physical, emotional, spiritual, intellectual and social needs of the service users while supporting them to have confidence to move on and live independently in the community.</p>
<button class="accordion"><strong>Learning disabilities are listed below:
Challenging behaviour</strong><i class="fa fa-plus"></i></button>
<div class="panel">
<ul>
<li>Autistic Spectrum</li>
<li>Attention Deficit Hyperactivity disorder (ADHD)</li>
<li>Asperger’s.</li>
<li>Dyslexic</li>
</ul>
</div>
<button class="accordion"><strong>Our service are:</strong><i class="fa fa-plus"></i></button>
<div class="panel">
<ul>
<li>Supporting with hygiene, chores and routines.</li>
<li>Companionship/support with routines and building self – esteem, behaviour management.</li>
<li>Supporting with daily living needs, travel, budgeting shopping</li>
<li>Supporting with inclusion and access community circles and interaction.</li>
<li>Supporting to be full citizens and be empowered to manage own finances.Voting and getting full citizenship</li>
<li>Access to health facilities : GP, Dental, Leisure</li>
<li>Paying rent and bills – and support to be out and about in the community, good neighbourhood and voting.</li>
<li>Tenancy right and responsibilities, application for disabled badge, and travel passes (bus).</li>
<li>Attending education, Skills for living, college University</li>
<li>Job Coaching , empowering with understanding of personal budgets or direct payments</li>
<li>Accessing Benefits, accessing advocacy, other rights as per individualized support and care needs.</li>
<li>Accessing Social activities /circles and inclusion: leisure</li>
<li>Paying rent and bills – and support to be out and about in the community risk assessment</li>
<li>Individualised interventions strategies. e.g. Self-confidence, I-Plan–it setting out goals, implementing and reviewing and evaluating.</li>
</ul>
</div>
</article>

@endsection