@extends('layouts.page')
@section('page-title')
Divine Healthcare  : Support service - Recovery
@endsection
@section('article-title')
Support services : Recovery
@endsection
@section('article-body')
<article >
<div class="entry-content">
<p><strong>Recovery means different things to different people.</strong></p>
<ul>
<li>It is recovering from a mental illness. It is about working towards personalised individuals’ goals and having hope for future.</li>
<li>It is very personal and Person centred approach is very important during the recovery support process. Recovery is not a one-off event – it is a process that can take time.</li>
<li>Recovery can mean for some, the differences between types of mental health recovery, and some research and resources.</li>
<li>Some researchers define recovery as, ‘Recovery isn’t about getting back to how you were before, it’s about building something new’.</li>
<li>We aim to provide Enablement skills so that the service user can independently in community.</li>
</ul>
<p><strong>We support recovery of individual in the following needs:</strong></p>
<ol type="A">
<li>Managing mental health</li>
<li>Physical health &amp; self -care</li>
<li>Daily Living skills</li>
<li>Social networks Work</li>
<li>Relationships Responsibilities</li>
<li>Addictive behaviour Identity and</li>
<li>Self – esteem Trust and</li>
<li>Hope</li>
</ol>
</div>
</article>
@endsection