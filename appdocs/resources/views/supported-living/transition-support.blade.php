@extends('layouts.page')
@section('page-title')
Divine Healthcare  : Transition support
@endsection
@section('article-title')
Transition support
@endsection
@section('article-body')
<article>
<div class="entry-content">
<p>Transitional support provides valuable reassurance for a person with learning disabilities who may be moving from a residential setting into supported living, or from their family home or supported living into their own home.</p>
<p>
<strong>Our Move on Policy: In–Reach Transition Support</strong>
<br>
Our move on policy meets the local authority personalisation agenda, we promoted rights for person centred delivery of support services by putting people first. We provide outreach for mental health and learning disabilities service users.</p>
<p>We deliver involvement policy, Service users benefit by moving seamlessly through life stages, by applying the principle of personalisation, to all that they do.</p>
</div>
</article>
@endsection