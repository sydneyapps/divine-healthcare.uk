@extends('layouts.page')
@section('page-title')
NHS Continuing Care - Divine Healthcare 
@endsection
@section('article-title')
NHS Continuing Care
@endsection
@section('article-body')
<p>If on-going care is required primarily for medical reasons and not principally because of frailty you may qualify for Continuous NHS care funding. NHS continuing healthcare is the name given to a package of services and funding for long term care which is paid for by NHS for people outside hospital with on-going health needs. You can get continuing healthcare in any setting, including your own home or in a care home.</p>
<p>Anyone assessed as requiring a certain level of care need can get NHS continuing healthcare. It is not dependent on a particular disease, diagnosis or condition, nor on who provides the care or where that care is provided.</p>
<p>On October 1st 2007 a new National Framework for NHS Continuing Care was introduced in England and with it a new Decision Support Tool to try and more fairly assess people’s entitlement to free continuing care through the National Health Service, based on their own needs and to try and prevent inconsistencies of decisions around the country over who qualified for this type of free funding for long term care.</p>
<p>The Primary Care Trust (PCT) in whose area your GP practice is located, is responsible for deciding your eligibility</p>
<p>Not everyone living at home or in a care home and who has on-going health needs is likely to qualify for free NHS care funding, but we would suggest that in the following situations you should ensure your eligibility for it has been assessed :</p>
<ul>
<li>If you have a rapidly deteriorating condition, which may be entering a terminal phase;</li>
<li>Before you are discharged from hospital, particularly if it seems a permanent place in a care home may be necessary;</li>
<li>When your care needs are being formally reviewed on a regular, usually annual, basis;</li>
<li>If your physical or mental health deteriorates significantly and your current care package seems inadequate</li>
<li>If you do not qualify for free care under HNS Continuing Care and need care in a care home, your Local Authority has a duty to financially assess you to see whether you need to pay for your own care or not – the so called Means Test.</li>
</ul>
<button class="accordion"><strong>The financial assessment</strong><i class="fa fa-plus"></i></button>
									<div class="panel">
<p>If your social services assessment assesses you as needing care in your own home, a care plan will be drawn up and the local authority will offer you a financial assessment (means test) to see if they would pay for the care or ask you to pay for it. You can decline it although this could mean that you will end up paying for all of it.</p>
<p>Not all services are charged –services of an occupational therapists and day centre visits are often free of charge, although transport to and from day centres would normally be charged.</p>
</div>
<button class="accordion"><strong>Means-testing</strong><i class="fa fa-plus"></i></button>
									<div class="panel">
<p>In assessing your financial circumstances if you have more than £23,250 (England 2010/11) in capital (not including the value of your home) you will be charged the full cost of your care. (N.B. Your home’s value is disregarded for domiciliary care but not if you have to enter a care home).</p>
<p>If your capital is less than £23,250 (England 2010/11), your income and any savings above the lower capital threshold (£14,000 England 2010/11) will be assessed but you must be left with at least the basic amount of Pension Credit plus 25%.</p>
<p>Capital between the Lower and Upper Capital Thresholds (£14,000 and £23,250 England 2010/11) is converted into tariff income at the rate of every £250 of capital between these two limits equal an extra £1 per week income.</p>
<p>The income of your partner or spouse and savings solely in their name is never included but half of any joint savings is counted as yours.</p>
<p>Please note: Unlike the means test for permanent residential care Local Authorities do have discretion to apply their own higher thresholds and you should check with your own local authority to see what they use.</p>
<p>Calculation</p>
<p>Income (including any tariff income) minus</p>
<p>actual costs incurred on any rent or mortgage payments</p>
<p>actual payments made for council tax</p>
<p>an allowance for general living costs (Generally Income Support or Pension Credit entitlement + 25%)</p>
<p>This produces a disposable income. You may then be asked to pay up to 95% of this disposable income towards the cost of care, although should your care costs require less than 95% you will only be asked to use as much as necessary.</p>
</div>
@endsection