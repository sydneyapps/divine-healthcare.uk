@extends('layouts.page')

@section('page-title')
Support Services - Divine Healthcare 
@endsection
@section('article-title')
Support Services
@endsection
@section('article-body')
										
				<div class="entry-content">
									<p><strong>We provide support services.</strong><br>
					We provide support services to people living in the community, either with their families or in their own homes independently. 
					</p>
					<p>
					We offer support services at an affordable cost.
					</p> 
						<p><strong>Our Services: </strong><br>
						<ul style="list-style-type: lower-alpha;">
						<li >Access to local community services: Preferred visit to access social activities 
						</li>
						<li> Cleaning: sweeping. Mopping the floor, cleaning walls, dusting 
						</li>
						<li> shopping: Accessing to preferred shops, 
						</li>
						<li> Escorting: escorting to preferred places, visiting friends, Gp, eye appointments and hospital appointments 
						</li>
						<li> Laundry:  Washing clothes, ironing and packing 
						</li>
						<li> Education: accessing for education facilities, skills need e.g. computer skills  
						</li><li>  Budgeting; Accessing to shopping needs and assisting with cost effective selection of affordable goods and needs.
						</li>
						<li> Home out door maintenance: gardening and disposing rubbish.
						</li>
						<li> Neighbourhood: relationships  
						</li>
						<li> Social Activities: gym, swimming, indoor and outdoor games
						</li></ul>
						</p>
				</div>
    @endsection