@extends('layouts.page')
@section('page-title')
In Control - Divine Healthcare 
@endsection
@section('article-title')
In Control
@endsection
@section('article-body')
<p>In Control is an initiative that champions self-directed support; it is supported by the Department of Health and many partner agencies. Most local authorities are working with In Control to establish self-directed support approaches in their areas.</p>
<p>The concept of self-directed support is the one that Divine Healthcare welcome the prospect of working with those who use our services and their families to implement Individual Budgets. However, with the increased empowerment comes more complexity in managing budgets which some may find daunting. We have the know-how to assist Service Users in the management of their care and finances. We can help take the complexity out of the process. </p><p>For more information on In Control please speak to them direct on <b>01564 821 650</b>. Alternatively you can visit their website www. in-control.org.uk. Remember, Divine Healthcare are here to help so do not hesitate to contact us on anything to do with your care.</p>
@endsection