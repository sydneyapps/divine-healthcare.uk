@extends('layouts.page')
@section('page-title')
Live in care - Divine Healthcare 
@endsection
@section('article-title')
Live in care
@endsection
@section('article-body')
			<article>
					<p>If you need someone to be with you at all times during the day and night but wants to remain independent in the comfort of your own home then look no further away from our live care packages.</p>
					<p>Live in care is an affordable alternative to residential or care home admission and is the perfect solution for you or your loved ones to remain in the one place that is familiar, accommodating and relaxed; your home.</p>
					<p>Experienced and dedicated live in care staff will live in your home and provide one to one individually tailored care either on a short or long term basis according to your wishes. Due to the delicate nature of this care package we take extra care to match our staff sympathetically with your situation and personality, as well as your needs. Live in carers will provide their own personal items and will not be dependent on you in any shape or form.<br>
					All care staff undergoes rigorous Criminal Record Bureau and employment checks and will be fully briefed on your preferences and expectations to them living in your home.</p>
			</article>
@endsection