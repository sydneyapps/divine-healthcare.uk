@extends('layouts.page')
@section('page-title')
Personal Budget - Divine Healthcare 
@endsection
@section('article-title')
Personal Budget
@endsection
@section('article-body')
<p>They are an allocation of funding given to users after an assessment which should be sufficient to meet their assessed needs. Users can either take their personal budget as a direct payment, or – while still choosing how their care needs are met and by whom – leave councils with the responsibility to commission the services. Or they can take have some combination of the two.<br>
As a result, they provide a potentially good option for people who do not want to take on the responsibilities of a direct payment. Personal budgets have been rolled out in England since 2008, with a target of providing every service user with one by 2013. </p><p>To find out more information about the support services we provide for Direct Payment Service Users and Personal Assistants please contact us on our free telephone <b>020 8665 4334</b> or email us <b>info@divine-healthcare.uk</b></p>
@endsection