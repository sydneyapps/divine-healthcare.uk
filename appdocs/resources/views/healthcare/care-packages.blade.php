@extends('layouts.page')

@section('page-title')
Care Packages - Divine Healthcare 
@endsection
@section('article-title')
Care Packages
@endsection
@section('article-body')
								<article id="post-102" class="post-102 page type-page status-publish hentry">
									<div class="entry-content">
										<h4>Mental Health</h4>	
										<p>Divine Healthcare  provides a range of community based mental health support services including low secure and step down facilities, supported living, residential care and independent hospitals, for adults with mental health needs.</p>
										<p>We support individuals with severe and enduring mental health needs such as alcohol and drug induced psychosis, schizophrenia and bi-polar disorder.</p>
										<p>The focus of our mental health support services is on rehabilitation and empowerment. This means that when an individual is placed in one of our services, we work with them to develop the skills and confidence needed to return to independent living as quickly as possible.</p>
										<h4>Respite Care</h4>
										<p>
										Respite Care is suitable for people in need of rehabilitation following a stay in the hospital or extra care following a period of illness. It also allows the sharing of care giving responsibility and getting support for the main care giver, be it a family member or friend. Seeking support and maintaining one’s own health is paramount and using respite care before becoming exhausted, isolated or overwhelmed is ideal. Just anticipating regular relief can become a lifesaver.</p>
										<h4>Learning Disabilities</h4>
										We are a provider that delivers high quality, flexible and person centred services to people with a learning disability. We have the right people with the right skills and all our staff really enjoy supporting people to lead fulfilling lives.</p>
									</div>
								</article>
@endsection