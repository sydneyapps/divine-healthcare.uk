@extends('layouts.page')
@section('page-title')
Social Service Funding - Divine Healthcare 
@endsection
@section('article-title')
Social Service Funding
@endsection
@section('article-body')
<p>Because of the extensive experience of our management, we are well versed in working within the policy frameworks of social services and other relevant agencies to ensure the needs of people who use our services are met.</p>
<h4>Providing value for money<strong><em></em></strong></h4>
<p><strong><em></em></strong>We recognise that those purchasing and commissioning social support services have to work to budgets, and local authorities can be assured that the services they purchase from Divine Healthcare’ are economic, effective and efficient.</p>
<p>We also aim to be innovative, providing the people who use our services with the best opportunities for choice giving purchasers and commissioner’s reassurance that they are investing in a reliable specialist provider.</p>
<p>We will always meet the council’s administrative requirements, for example the provision of tailored reports which fulfil a specific need. We maintain a regular dialogue with all persons who matters and value feedback from them, an important driver to maintaining and improving quality.</p>
@endsection