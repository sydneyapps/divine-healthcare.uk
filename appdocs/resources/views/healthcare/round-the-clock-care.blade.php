@extends('layouts.page')
@section('page-title')
Sleeping Nights- Divine Healthcare 
@endsection
@section('article-title')
Sleeping Nights
@endsection
@section('article-body')
					
<article class="">
<p>Night care might be required to provide a break for your family or primary carer or for a short duration while you recover from an accident, illness or hospital stay. As with all our services, we can adapt to meet your specific needs.</p>
<p>Waking Nights or Sleepovers: one of our care staff will come to your home for the night hours and remain awake to provide whatever assistance or monitoring is required.</p>
<p>Non-waking or Sleeping Nights – one of our care staff will sleep in your home during the night hours to provide peace of mind and assistance if required.</p>
</article>
@endsection