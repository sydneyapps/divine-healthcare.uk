@extends('layouts.page')
@section('page-title')
Recipite and recare - Divine Healthcare 
@endsection
@section('article-title')
Recipite and recare
@endsection
@section('article-body')
<button class="accordion"><strong>Rest and Recharge
</strong><i class="fa fa-plus"></i></button>
	<div class="panel">
<p>The role you’ve taken as a family caregiver is an important one. Your loved one and other family members depend on you. As the primary caregiver, not only do you deserve an occasional break, but also it’s really necessary so you can recharge your physical, spiritual and emotional batteries. It’s critical that you take the time to care for yourself — doctor’s appointments, regular physical exams, counseling sessions or support group meetings — to ensure you’re not getting rundown and that you’ll be up to the challenges of this daunting responsibility.</p>
<p>Divine Healthcare are ready to step in with the same considerate care and concern for your loved one’s needs so you can get away for any reason. Whether you need to shop, attend a child’s school event, go to church, get some much needed exercise or just have lunch with a friend, our respite support services allow you to relax and enjoy, knowing your family member is well cared for in your absence. Our respite care program is also a wonderful solution when you go on vacation, travel for the holidays, or attend an out-of-town event such as a wedding or reunion.</p>
</div>
<button class="accordion"><strong>Tailored to Meet Your Needs
</strong><i class="fa fa-plus"></i></button>
	<div class="panel">
<p>Divine Healthcare can provide respite care daily, weekly or as needed. We are happy to arrange care for just a few hours, overnight or 24-hour care while you’re away.</p>
</div>
<button class="accordion"><strong>Find Time for Yourself
</strong><i class="fa fa-plus"></i></button>
	<div class="panel">
<p>Our service gives you time to relax, pursue activities that give you joy and rediscover yourself. While respite care can’t change your loved one’s situation, it can help you maintain your sense of being and significantly improve your ability to deal with the situation and handle challenges in a positive way.</p>
</div>
@endsection