@extends('layouts.page')

@section('page-title')
Companion/Sitting Services - Divine Healthcare 
@endsection
@section('article-title')
Companion/Sitting Services
@endsection
@section('article-body')														
									<article id="post-97" class="post-97 page type-page status-publish hentry">
											<div class="entry-content">
												<p><strong>Do you need someone to keep you company now and again, or on a regular basis?</strong><br>
												<strong>Are you concerned about a relative or loved one who gets lonely?</strong><br>
												<strong>Do you live some distance away from a relative or loved one and would like someone to visit them?</strong><br>
												<strong>Are you a main carer who would like someone to step in from time to time so you can have a break?</strong></p>
												<h4>Then let us help.</h4>
												<p>Our Companionship / Sitting Service typically cover things like:</p>
												<ul>
												<li>Transport to appointments or social engagements e.g. hairdresser, doctor, dentist, cinema, friend’s house</li>
												<li>Accompanying to appointments or social engagements</li>
												<li>Company for a walk or other outdoor activity</li>
												<li>Helping with hobbies / interests</li>
												<li>Reading books / newspapers out loud</li>
												<li>Being there to talk and listen</li>
												</ul>
												<p>To find out more about our Companionship services, please contact us on <b>020 8665 4334</b> or email us at <b><a href="mailto:info@divine-healthcare.uk">info@divine-healthcare.uk</a></b></p>
											</div>	
										</article>
@endsection