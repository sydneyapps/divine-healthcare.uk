@extends('layouts.page')
@section('page-title')
Divine Healthcare  : Education - Loans for 24 plus
@endsection
@section('article-title')
Education : Loans for 24 plus
@endsection
@section('article-body')
<article>
<p>Loans are available for 24 plus training. They are payable in 2016. This is an opportunity for achieving your goals. Training covers a range of courses which include Apprenticeships, Health and Social care, Children and young person workforce, Business administration and Customer Services.</p>
    <p>For further information visit: 
    <a title="24+ Advanced Learning Loans" href="https://www.gov.uk/advanced-learning-loans/further-information" target="_blank">
    <strong>24+ Advanced Learning Loans – GOV.UK</strong> </a>
    https://www.gov.uk/advanced-learning-loans/further-information
    </p>s
</article>
@endsection