@extends('layouts.page')
@section('page-title')Education - Training
@endsection
@section('article-title')
Education : Training
@endsection
@section('article-body')
<article>

<p>
Care providers across the UK trust us to deliver assessment for apprenticeships and high-quality training at all levels for their staff.
<p></p>We specialize in the assessments in Health & Social Care sector and work closely with national care providers to deliver high quality QCF training and assessments via our Apprenticeship Programmed and stand-alone Diplomas and Certificates.
<p>
With expert knowledge of the Apprenticeship Levy we work to ensure your business gains cost effective access to the most appropriate training and ongoing assessment. Our nationwide team of highly experienced trainers and assessors work to ensure that the training requirements of your staff are fully realised.
</p>We specialize in delivering and assessing apprenticeships and courses for your business, including:
<ul>
<li>Skills for care certificate training and assessment </li>
<li>Full Health & Social Care Apprenticeships designed to ensure you gain maximum benefit from your Apprenticeship Levy assessment.
</li><li>Convenient short courses to enable your staff to develop their knowledge of Dementia, Stroke Care or Parkinson’s, First Aid, Manual Handling, POVA, SOVA, End of Life Care, Medication Awareness and more.
</li>
</ul>
<p>Divine Motions provide a full QCF Level 2 as part of our Apprenticeship Programmed and assessment. Further QCF modules are available, these are a convenient and cost-effective way to support ongoing staff development.
</p>

<!-- 
<p><b>We offer apprenticeships in the following sector:</b>
<ul><li>Health & Social Care</li></ul>
</p> 
-->

</p>
</article>
@endsection