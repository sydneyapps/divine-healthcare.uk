@extends('layouts.page')
@section('page-title')
Divine Healthcare  : Education - Loan  for 24 plus
@endsection
@section('article-title')
Loan  for 24 plus
@endsection
@section('article-body')

<article>
		<p>Divine Healthcare is more than just a Center</p>
        <p>We provide quality teaching and offer support to all individuals with different needs.<br>
        Our support teams are available at all the time and stages: Our support is continuous during your starting period, process of learning and until you achieve outcomes.</p>
        <p><strong>Are you thinking of joining us at our center?</strong></p>
        <p>Visit our website and its information; we support you in choosing the course that’s right for you. We support with course advice. We support you with initial information about funding courses and pathways to job opportunities.</p>
        <button class="accordion"><strong>If you are a Student</strong><i class="fa fa-plus"></i></button>
        <div class="panel">
        We are available to give support at your interview or assessment if needed. We have assistance for candidates with disabilities or additional learning needs such as:<br>
        • Individual learning needs support<br>
        • Dyslexia support from our qualified team</p>
</div>
        <button class="accordion"><strong>Are you already a student at Divine Healthcare?</strong><i class="fa fa-plus"></i></button>
        <div class="panel">
        The Center has policies to meet equal opportunities along with grievance, bullying and harassment and whistleblowing policies to combat discriminatory attitudes and practices whether these are expressed by individuals or through institutional practices.</p>
        <p>The Center is committed to a programme of positive action to make these policies effective; to monitoring and evaluating its progress and to targeting areas for improvement.</p>
        </div>
        <button class="accordion"><strong>The Center also wishes to champion diversity and inclusion by ensuring that:</strong><i class="fa fa-plus"></i></button>
    <div class="panel">
        • We create a visibly diverse environment which values difference and raises aspiration<br>
        • We offer flexible opportunities which meet local learning need and enable all students to realise their potential<br>
        • All staff are clear about standards and strategies to meet diverse learner needs and are equipped to respond effectively<br>
        • We undertake open monitoring of learner and staff performance and experience to identify and act on equality gaps<br>
        • We have a comprehensive system to capture feedback of learner, staff and community of how we are doing and what we could improve<br>
        • We respect and value learners’ staff and community feedback to remain alert to patterns of inequality and related concerns which are not identified through statistical monitoring tools.</p>
</div>
<button class="accordion"><strong>About the accademy</strong><i class="fa fa-plus"></i></button>
<div class="panel">
        <p>Divine Healthcare is a safe environment for learning and our delivery meet the needs of the learners and promote the community and economic needs.<br>
        We have a Safe guarding Policy.<br>
        We take your well-being and safety seriously</p>
        
        <p>Our procedures ensure that you feel safe while you are at College. Staff are appropriately recruited and trained so that we can take immediate action should there ever be concerns about your welfare. We maintain zero tolerance drugs policy and smoking is not allowed on our premises.</p>
        <p>a) Our mission is to create a community that has a successful future and that will help build the economy.</p>
        <p>b) Our Vision is to create a successful a well trained work force in all areas of the industry with world class skills</p>
        <p>c) Our Values<br>

        i) Equality and excellency<br>
        ii) Inspiring, innovative and inclusive<br>
        iii) Successful and supportive<br>
        iv) Equal opportunities for all<br>
        v) Professional integrity<br>
        vi) Support learners throughout until they achieve their goal<br>
        vii) We will endeavour to be excellent in what we do.</p>
        <p>d) Employment services – please visit&nbsp;<a href="http://www.theskillspeople.org/">http://www.theskillspeople.org/</a></p>
</div>
</article>

@endsection