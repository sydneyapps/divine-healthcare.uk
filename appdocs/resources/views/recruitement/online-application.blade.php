@extends('layouts.intro-page')

@section('page-title')
Recruitment Online Application - Divine Healthcare 
@endsection
@section('headerscript')
<script type="text/javascript">
	var a = Math.ceil(Math.random() * 10);
	var b = Math.ceil(Math.random() * 10);       
	var c = a + b

	function DrawBotBoot()
	{
		document.write("What is "+ a + " + " + b +"? ");
		
	}    
	function ValidBotBoot(){
		var d = document.getElementById('BotBootInput').value;
		if (d == c) return true;        
		return false;
	}
</script>
@endsection
@section('slider-bar-top')
    @parent
    <form action="" method="post" role="form" id="ajax-contact-form"  class="contactForm lead">
        <div class="row">
        
				<div class="col-lg-6">
                    <div class="wow fadeInDown" data-wow-offset="0" data-wow-delay="0.1s">
			        <h2 class="h-ultra">Online Application</h2>
                    </div>
                </div>
                <div class="col-lg-6">
				
                </div>
		</div>								
    <div class="col-lg-6">
	<div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.1s">
			<h4 class="h-light"></h4>
		</div>
	<div class="well well-trans">
			<div class="wow fadeInRight" data-wow-delay="0.1s">
			<ul class="lead-list">
			<li><span class="fa fa-user-md fa-2x icon-link"></span> <span class="list"><strong>Health Care Assistants</strong><br />We are recruiting for care assistancts </span></li>
			<li><span class="fa fa-wheelchair fa-2x icon-link"></span> <span class="list"><strong>Support workers</strong><br />Various support workers required</span></li>
			<li><span class="fa fa-book fa-2x icon-link"></span> <span class="list"><strong>CV Template</strong><br /><a href=/download/cv/cv-library-general-cv-template.docx>Click here to get a cv and apply</a></span></li>
			</ul>
			</div>
        </div>
    </div>
    <div class="col-lg-6">
		
		<div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.1s">
			<h4 class="h-light"></h4>
		</div>
        <div class="well well-trans">
			<div class="wow fadeInRight" data-wow-delay="0.1s">
			<div class="row">
    											<div class="col-xs-6 col-sm-6 col-md-6">
    												<div class="form-group">
    													<label>Full Name</label>
    													<input type="text" name="name" id="name" class="form-control input-md" data-rule="minlen:3" data-msg="Please enter at least 3 chars">
                                                        <div class="validation"></div>
    												</div>
    											</div>
												<div class="col-xs-6 col-sm-6 col-md-6">
    												<div class="form-group">
    													<label>Email</label>
    													<input type="text" name="email" id="email" class="form-control input-md" data-rule="minlen:3" data-msg="Please enter an email">
                                                        <div class="validation"></div>
                                                    </div>
    											</div>
            </div>
            
            <div class="row">
												<div class="col-xs-6 col-sm-6 col-md-6">
    												<div class="form-group">
    													<label>Phone</label>
    													<input type="text" name="phone-kin" id="kin-phone" class="form-control input-md" data-rule="minlen:3" data-msg="Please enter an email">
                                                        <div class="validation"></div>
                                                    </div>
    											</div>
    											<div class="col-xs-6 col-sm-6 col-md-6">
    												<div class="form-group">
    													<label>Town</label>
    													<input type="text" name="kin" id="kin" class="form-control input-md" data-rule="minlen:3" data-msg="Please enter at least 3 chars">
                                                        <div class="validation"></div>
    												</div>
    											</div>
    		</div>
            <div class="row">
    											<div class="col-xs-6 col-sm-6 col-md-6">
    												<div class="form-group">
    													<label>Attach your cv</label>
    													<input type="file" name="postcode" id="postcode" class="form-control input-md" data-rule="minlen:3" data-msg="Please enter an email">
                                                        <div class="validation"></div>
    												</div>
    											</div>
												<div class="col-xs-6 col-sm-6 col-md-6">
    												<div class="form-group">
    													<label></label>
    													<input type="submit" name="cv" id="cv" class="form-control input-md" data-rule="minlen:3" data-msg="Please enter at least 3 chars">
                                                        <div class="validation"></div>
                                                    </div>
    											</div>
    		</div>

			</div>
        </div>
    </div>
                                </form>
@endsection
@section('slider-bar-bottom')
    @parent
    @endsection
@section('home-content')
    <section id="partner" class="home-section">	
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="home-section">
						<h2 class="h-bold">Who we are </h2>
					<p>
						We are a registered Domiciliary Care provider, offering Community Supported Living to people living in the community, either with their families or in their own homes.
						</p><p>
						We offer support with personal care, as well as help and support for each person to learn new skills, develop independence, access other services and enjoy life as part of the community.
						</p>
						<p>
						We provide home based support to individuals who are referred by Social Services or who are purchasing services through direct payment. or Individual Budget.
						</p>
						<p>
						Each person has a Support Plan and an Activity Planner. Information includes personal care, learning opportunities, recreational, social, respite breaks, vocational and work activities.
						</p>
						<p>
						We provide care and support services to the following Service User groups
						</p>
						<p>
						Mental Health
						Elderly people
						People with physical disabilities
						People with learning disabilities
						People who require palliative care
						People recently out of hospital and are recovering after a long illness.
					</p>
					</div>
				</div>
			</div>
		</div>
    </section>
    @endsection
	@section('scripts')
		<script src="{{ asset('js/contact_form.js') }}"></script>
	@endsection
