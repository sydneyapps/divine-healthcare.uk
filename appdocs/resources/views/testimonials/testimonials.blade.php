@extends('layouts.intro-page')

@section('page-title')
Testimonials - Divine Healthcare 
@endsection

@section('headerscript')
	<link href='https://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>

<script type="text/javascript">
	var a = Math.ceil(Math.random() * 10);
	var b = Math.ceil(Math.random() * 10);       
	var c = a + b

	function DrawBotBoot()
	{
		document.write("What is "+ a + " + " + b +"? ");
		
	}    
	function ValidBotBoot(){
		var d = document.getElementById('BotBootInput').value;
		if (d == c) return true;        
		return false;
	}
</script>
@endsection
@section('slider-bar-top')
	@parent
	<div class="col-lg-6">
		<div class="wow fadeInDown" data-wow-offset="0" data-wow-delay="0.1s">
			<h2 class="h-ultra">Send Your Testimony</h2>
		</div>
		<div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.1s">
			<h4 class="h-light"></h4>
		</div>
		<div class="well well-trans">
			<div class="wow fadeInRight" data-wow-delay="0.1s">
			<ul class="lead-list">
			<li><span class="fa fa-phone fa-2x icon-link"></span> <span class="list"><strong>Contact Number / Out of hours</strong><br />+44 (0) 208 665 4334 </span></li>
			<li><span class="fa fa-envelope fa-2x icon-link"></span> <span class="list"><strong>Email Address</strong><br />info@divine-healthcare.uk</span></li>
			<li><span class="fa fa-calendar fa-2x icon-link"></span> <span class="list"><strong>Open Times</strong><br />Monday - Sunday, 24hr</span></li>
			</ul>
			</div>
		</div>
	</div>

	<div class="col-lg-6">
					<div class="panel-body">
										
										<form action="" method="post" role="form" id="ajax-email-form" data-url="/contact-validation" class="contactForm">
											<div  class="row">
												<div class="col-xs-12 col-sm-12 col-md-12">
													<div class="form-group">
														<span class="lead-footer">* We'll contact you by phone &amp; email later</span>
														<div id="errormessage"></div>
														<div id="sendmessage">Your message has been sent. Thank you!</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-6 col-sm-6 col-md-6">
													<div class="form-group">
														<label>Full Name</label>
														<input type="text" name="first_name" id="f1" class="form-control input-md" data-rule="minlen:3" data-msg="Please enter at least 3 chars">
														<div class="validation"></div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-6 col-md-6">
													<div class="form-group">
														<label>Email</label>
														<input type="text" name="email" id="f3" class="form-control input-md" data-rule="minlen:3" data-msg="Please enter an email">
														<div class="validation"></div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-12">
													<div class="form-group">
														<label>Message</label>
														<textarea type="textfield" name="message" id="f5" class="form-control input-md" data-rule="required" data-msg="The message is required"></textarea>
														<div class="validation"></div>
													</div>
												</div>
											</div>
											
											<div class="row">
												<div class="col-xs-6 col-sm-6 col-md-6">
														<label for="f6"><script type="text/javascript">DrawBotBoot()</script></label>
														<input type="text" name="captcha-value" id="BotBootInput" class="form-text"  maxlength='2' size='2' value="" size="40">                                                        

												</div>
												<div class="col-xs-6 col-sm-6 col-md-6">
														<input type="submit" value="Submit" class="btn btn-skin btn-block btn-lg">
												</div>
											</div>
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
										</form>
					</div>
	</div>						
@endsection
@section('slider-bar-bottom')
@parent
@endsection
@section('home-content')
<section id="partner" class="home-section">	
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="home-section">
					<h2 class="h-bold">Testimonials</h2>
				</div>
			</div>
			<div class="col-md-12 paddingtop-40">
						<div id="main-display">
							<div class="entry-content" role="main">
								<button class="accordion"><strong>Watch testimonials
								</strong><i class="fa fa-plus"></i></button>
								<div class="panel">
									<div class="row">
										<div class="col-xs-6 col-sm-6 col-md-6">
											<object width="433" height="220" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"><param name="allowFullScreen" value="true"><param name="allowscriptaccess" value="always"><param name="src" value="http://www.youtube.com/v/JtNDeOLCClM?version=3&amp;hl=en_GB&amp;rel=0"><param name="allowfullscreen" value="true"><param name="wmode" value="transparent"><embed width="433" height="220" type="application/x-shockwave-flash" src="http://www.youtube.com/v/JtNDeOLCClM?version=3&amp;hl=en_GB&amp;rel=0" allowfullscreen="true" allowscriptaccess="always" wmode="transparent"></object>
										</div>
										<div class="col-xs-6 col-sm-6 col-md-6">
											<object width="433" height="220" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"><param name="allowFullScreen" value="true"><param name="allowscriptaccess" value="always"><param name="src" value="http://www.youtube.com/v/1AUwESTi2as?version=3&amp;hl=en_US"><param name="allowfullscreen" value="true"><embed width="433" height="220" type="application/x-shockwave-flash" src="http://www.youtube.com/v/1AUwESTi2as?version=3&amp;hl=en_US" allowfullscreen="true" allowscriptaccess="always"></object>
										</div>
									</div>
								</div>
								<button class="accordion"><strong>Read testimonials
								</strong><i class="fa fa-plus"></i></button>
								<div class="panel">
									<p>My name is Cynthia and would like to air my views over Divine Motions good customer service. I can bet you that you will never go wrong by being their client. I feel respected and honoured whenever I communicate with them.<br>
									Thank you once again</p>
									<p><strong><br>
									Cynthia <!-- Sharon Eden --> </strong></p>
									<hr>
									<p>I have been one of your clients since 2009 and I must say your customer service support is really excellent. I have actually recommended a number of friends to join/ use you because of your support.<br>
									Keep up the good work and keep your customers happy.</p>
									<p><strong>Brighton Marimo&nbsp;</strong><br>
								</div>
								</div>
								<button class="accordion"><strong>Listen to testimonials
								</strong><i class="fa fa-plus"></i></button>
								<div class="panel">
								
							</div>
						</div>
			</div>
			</div>
		</div>
</section>
@endsection
@section('scripts')
	<script type="text/javascript" src="{{ asset('/js/contact_form.js') }}"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5NTW3wmh3NDqkiehT0Ad_4nobp13oMAo"></script>
@endsection
