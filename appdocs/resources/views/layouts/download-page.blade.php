@extends('layouts.base')
@section('page-title')
@yield('title')
@endsection
@section('content')
<section id="intro" class="intro">
		<div class="intro-content intro-nobg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="home-section">
							<div class="section-heading">
								<h2 class="h-bold">@yield('article-title')</h2>
							</div>
						</div>
					</div>
					@yield('article-body')
				</div>										
		</div>
</section>
@endsection