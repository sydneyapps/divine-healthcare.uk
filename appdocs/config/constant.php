<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [
    
    /**
     * Response errors
     */
    'msg'=>[
        'success'=>'Success',
        'notfound' =>'Not found',
        'mailing_error'=>'Sending mail failed',
        'sent'=> 'Message was successfully sent.',
        'validation_failure'=>'Validation failed',
        'missing'=>'Missing required fields.'
    ],
    'status'=>[
        'success'=>200,
        'notfound'=>100,
        'maing_error'=>101,
        'validation_failure'=>102
    ],
    
    /**
     * Contact form validation Messages. 
     */
    'form'=>[
        'email'=>'Invalid email',
        'name'=>'Name is required.',
        'message'=>'Message is required.',
        'CONTACT_WEBMAIL'=>'Contact us webmail',
        'password'=>'Password is required'
        ]
];