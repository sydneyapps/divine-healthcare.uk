<?php

namespace App\Http\Controllers;

use App\Model\Downloadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Response;

class downloadController extends Controller
{
 
    const DOWNLOADS_VIEW_FOLDER = "downloads";

    public function download($f,$e){
        
       $contents = Storage::disk('public')->get("$e/$e.$f");       
        return Response::make( $contents,200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.str_replace('-','',ucfirst($e)).'"'
        ]);
    }

    public function index($downloads){
        return $this->getView($downloads);
    }

    private function getView( $dnload){
        
        $files = Storage::allFiles('public/'.$dnload);
        //echo sizeof($files);
        $url = Storage::disk('public')->url('healthcare/healthcare.pdf');       
        $downloads = array();

        if(sizeof($files)> 0){
            for($x = 0; $x < count($files); $x++) {        
                $downloads[$x] = new Downloadable( $files[$x]);
            }
        }
        
        return view(self::DOWNLOADS_VIEW_FOLDER.'.'.$dnload, ['files'=>$downloads]);
    
    }
}