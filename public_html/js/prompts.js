/* var vid = document.getElementById("prompts");
$('#prompts').on('canplay', function() {
    $("#prompts").get(0).play();
    setTimeout(
        function() 
        {
          console.log("do something special");
        },
        2000);
    if ($("#prompts").get(0).paused) {
       console.log("Its paused.");   
    } else {
        $("#prompts").get(0).pause();     
        console.log("Its was playing.");
    }
});
*/

(function ( $ ) {  
    
    $.fn.videoPlay = function( options ) {
        var playing = false;
        var playTime =0;
        var settings = $.extend({
            duration: 0,
             currentImage: 0,
            imageScreenDuration: [10,6,10,8,6,31]
        }, options );
        
        let audelem = document.getElementById("prompts");
        audelem.addEventListener('play', function() { playing=true; });
        audelem.addEventListener('pause', function() { playing=false; });
        audelem.addEventListener('seeked', resetVideo(Math.floor(audelem.currentTime)));
        
        function resetVideo(playTime){
            console.log(playTime);
            prev  = 0;
            next =0;
            found = false;
            for (i = 0; i < settings.imageScreenDuration.length; i++) {               
                if(!found){
                    prev = next;
                    next = prev + settings.imageScreenDuration[i];

                    if(playTime < next ){
                        found = true;
                        if(settings.currentImage != i){
                            console.log(settings.currentImage);
                            $('.slide-'+settings.currentImage).toggleClass("active");
                            settings.currentImage = i;
                            $('.slide-'+settings.currentImage).toggleClass("active");
                            settings.duration = playTime - prev;
                        }                        
                    } 
                }
            }
        }

        window.setInterval(function() {
            
            if(playing){
                elapsedTime = Math.floor(audelem.currentTime);
                console.log(elapsedTime);
                settings.duration ++;

                if(settings.duration == settings.imageScreenDuration[settings.currentImage]){
                    settings.duration = 0;
                    $('.slide-'+settings.currentImage).toggleClass("active");
                    settings.currentImage ++;
                    if(settings.currentImage > settings.imageScreenDuration.length -1){
                        settings.currentImage = 0;
                    }
                    $('.slide-'+settings.currentImage).toggleClass("active");
                }
            }  
        }, 1000);     
    };  
    $('#prompts').videoPlay({});
}( jQuery ));



